import React, { Component } from 'react';
import { withHeaderTitle } from '../../components/Header/HeaderTitle';
import { Card, CardBody, CardHeader, CardTitle } from 'reactstrap';

import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import bootstrapPlugin from '@fullcalendar/bootstrap';

import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import '@fullcalendar/list/main.css';
import '@fullcalendar/bootstrap/main.css';

import './Calendar.scss';

import events from './Calendar.events';

// Custom for use with no FA icons
bootstrapPlugin.themeClasses.bootstrap.prototype.baseIconClass = '';
bootstrapPlugin.themeClasses.bootstrap.prototype.iconOverridePrefix = '';

class Calendar extends Component {
    calendarEvents = events;

    calendarPlugins = [
        interactionPlugin,
        dayGridPlugin,
        timeGridPlugin,
        listPlugin,
        bootstrapPlugin
    ];

    calendarHeader = {
        left: 'dayGridMonth,timeGridWeek,timeGridDay',
        center: 'title',
        right: 'prev,next'
    };

    customIcons = {
        close: 'ion-close-round',
        prev: 'ion-chevron-left',
        next: 'ion-chevron-right'
    };

    // External events properties

    state = {
        selectedEvent: null,
        evRemoveOnDrop: false,
        evNewName: '',
        externalEvents: [
            { color: 'red-500', name: 'Lunch' },
            { color: 'pink-500', name: 'Go home' },
            { color: 'purple-600', name: 'Do homework' },
            { color: 'blue-500', name: 'Work on UI design' },
            { color: 'light-green-500', name: 'Sleep tight' }
        ]
    };

    componentDidMount() {
        /* initialize the external events */
        new Draggable(this.refs.externalEventsList, {
            itemSelector: '.fce-event',
            eventData: function(eventEl) {
                return {
                    title: eventEl.innerText.trim()
                };
            }
        });
    }

    dayClick = date => {
        this.setState({
            selectedEvent: {
                date: date.dateStr
            }
        });
    };

    handleEventReceive = info => {
        var styles = getComputedStyle(info.draggedEl);
        info.event.setProp('backgroundColor', styles.backgroundColor);
        info.event.setProp('borderColor', styles.borderColor);

        // is the "remove after drop" checkbox checked?
        if (this.state.evRemoveOnDrop) {
            this.removeExternalEvent(info.draggedEl.textContent);
        }
    };

    removeExternalEvent = name => {
        let externalEvents = [...this.state.externalEvents];
        const index = externalEvents.findIndex(e => e.name === name);
        if (index > -1) {
            externalEvents.splice(index, 1);
            this.setState({
                externalEvents
            });
        }
    };

    handleCheck = event => {
        this.setState({
            evRemoveOnDrop: event.target.checked
        });
    };

    render() {
        const { externalEvents, selectedEvent } = this.state;
        return (
            <section className="section-container">
                <div className="container container-fluid">
                    <div className="calendar-app">
                        <div className="row">
                            <div className="col-xl-3 col-lg-4">
                                <div className="row">
                                    <div className="col-lg-12 col-12">
                                        {/* START card */}
                                        <Card className="card-default mb-3" title="">
                                            <CardHeader>
                                                <CardTitle tag="h4" className="m-0">
                                                    Events
                                                </CardTitle>
                                            </CardHeader>
                                            {/* Default external events list */}
                                            <CardBody>
                                                <div
                                                    className="external-events"
                                                    ref="externalEventsList"
                                                >
                                                    {externalEvents.map(ev => (
                                                        <div
                                                            className={'fce-event bg-' + ev.color}
                                                            key={ev.name + ev.color}
                                                        >
                                                            {ev.name}
                                                        </div>
                                                    ))}
                                                </div>
                                                <div className="custom-control custom-checkbox mt-3">
                                                    <input
                                                        className="custom-control-input"
                                                        id="drop-remove"
                                                        type="checkbox"
                                                        onChange={this.handleCheck}
                                                    />
                                                    <label
                                                        className="custom-control-label"
                                                        htmlFor="drop-remove"
                                                    >
                                                        Remove after Drop
                                                    </label>
                                                </div>
                                            </CardBody>
                                        </Card>
                                        {/* END card */}
                                    </div>
                                </div>
                                <div className="mb-3">
                                    {selectedEvent && (
                                        <div>
                                            <p>Selected:</p>
                                            <div className="box-placeholder">
                                                {JSON.stringify(selectedEvent)}
                                            </div>
                                        </div>
                                    )}
                                    {!selectedEvent && (
                                        <div>
                                            <p>Click calendar to show information</p>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="col-xl-9 col-lg-8">
                                <Card className="card-default">
                                    <CardBody>
                                        {/* START calendar */}
                                        <FullCalendar
                                            defaultView={this.dayGridMonth}
                                            plugins={this.calendarPlugins}
                                            events={this.calendarEvents}
                                            themeSystem={'bootstrap'}
                                            header={this.calendarHeader}
                                            editable={true}
                                            droppable={true}
                                            deepChangeDetection={true}
                                            dateClick={this.dayClick}
                                            eventReceive={this.handleEventReceive}
                                            iconOverrideOption={'bootstrapFontAwesome'}
                                            bootstrapFontAwesome={this.customIcons}
                                        />
                                    </CardBody>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withHeaderTitle(Calendar);
