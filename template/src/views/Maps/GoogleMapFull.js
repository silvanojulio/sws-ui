import React, { Component } from 'react';
import { withHeaderTitle } from '../../components/Header/HeaderTitle';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

const mapContainerStyle = {
    height: '100%'
};

const APIKEY = 'AIzaSyBNs42Rt_CyxAqdbIBK0a5Ut83QiauESPA';

const DemoMapCustomType = props => (
    <LoadScript id="script-loader" googleMapsApiKey={APIKEY}>
        <GoogleMap
            zoom={4}
            center={props.location}
            mapContainerStyle={mapContainerStyle}
            mapContainerClassName="fh"
        >
            <Marker position={props.location} />
        </GoogleMap>
    </LoadScript>
);

class GoogleMapFull extends Component {
    state = {
        location: {},
        myMarkers: [
            {
                id: 0,
                name: 'Canada',
                coords: { lat: 56.130366, lng: -106.346771 }
            },
            {
                id: 1,
                name: 'New York',
                coords: { lat: 40.712784, lng: -74.005941 }
            },
            {
                id: 2,
                name: 'Toronto',
                coords: { lat: 43.653226, lng: -79.383184 }
            },
            {
                id: 3,
                name: 'San Francisco',
                coords: { lat: 37.774929, lng: -122.419416 }
            },
            {
                id: 4,
                name: 'Utah',
                coords: { lat: 39.32098, lng: -111.093731 }
            }
        ]
    };

    constructor(props) {
        super(props);
        this.props.setHeaderTitle('Google Maps Full');
    }

    centerMapAt = location => node => {
        console.log('Centering at: ' + location);
        this.setState({
            location
        });
    };

    componentDidMount() {
        this.setState({
            location: this.state.myMarkers[0].coords
        });
    }

    render() {
        return (
            <section className="section-container">
                <div className="container-full">
                    <div className="row fh bg-white">
                        <div className="col-lg-3 fh d-none d-sm-block d-none d-sm-block pr0">
                            <div className="p-lg">
                                <h5 className="text-center">Places</h5>
                                <p className="text-center">
                                    Click on each item to focus a marker in the map
                                </p>
                            </div>
                            <div className="list-group list-group-unstyle ">
                                {this.state.myMarkers.map(m => (
                                    <div
                                        className={
                                            'list-group-item justify-content-between ' +
                                            (m.coords === this.state.location ? 'active' : '')
                                        }
                                        onClick={this.centerMapAt(m.coords)}
                                        key={m.id}
                                    >
                                        {m.name}
                                        <em className="float-right ion-ios-arrow-forward" />
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="col-lg-9 fh pl0">
                            <DemoMapCustomType location={this.state.location} mapType="satellite" />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withHeaderTitle(GoogleMapFull);
