import React, { Component } from "react";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { withTranslation, Trans } from 'react-i18next';
import { withHeaderTitle } from '../../components/Header/HeaderTitle';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import EasyPieChart from 'easy-pie-chart';
import '../../components/Charts/EasyPieChart.scss';

import Sparkline from '../../components/Common/Sparkline';
import Colors from '../../components/Colors/Colors';
import FlotChart from '../../components/Charts/Flot';
import VectorMap from '../../components/Maps/VectorMap';

class Dashboard extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <section className="section-container">
                Home    
            </section>
        );
    }
}

export default withTranslation('translations')(withHeaderTitle(Dashboard));
